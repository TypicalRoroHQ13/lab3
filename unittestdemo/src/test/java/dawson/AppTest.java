package dawson;

import static org.junit.Assert.*;
import org.junit.Test;

public class AppTest 
{
    
    
    @Test
    public void shouldEqualto5(){
        assertEquals("It is not 5", 5, App.echo(5));
    }
    @Test
    public void shouldBeOneMore(){
        assertEquals("not equals to 6", 6, App.oneMore(5));
    }
}
